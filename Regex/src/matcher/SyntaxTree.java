package matcher;
/**
 * Class SyntaxTree represents syntax trees, which we construct after given regular expressions. A syntax tree is then translated into the corresponding automaton, defined in {@link Automat}.
 * 
 * @author Nils Goeritz (313780), Patrick Finke (343628), Bella Naumova (318299)
 * @version 1.0
 */
public class SyntaxTree {
	
	/**
	 * Root of the syntax tree.
	 */
	public SyntaxNode root;
	
	/**
	 * Constructs a new syntax tree.
	 * @param r A syntax node to be assigned as the root of the new syntax tree.
	 */
	public SyntaxTree(SyntaxNode r) {
		root = r;
	}
	
	/**
	 * Getter for the root
	 * @return root
	 */
	public SyntaxNode getRoot() {
		return this.root;
	}
	
	/**
	 * Constructs a syntax tree from a given regular expression by calling {@link #parse(String, Integer)} for the first position in the regex.
	 * @param regex Regular expression to be parsed.
	 * @return SyntaxTree
	 */
	public static SyntaxTree regexToSyntaxTree(String regex) {
		Integer i=0;
		Pair<SyntaxTree, Integer> res=parse(regex, i);
		return res.a;
	}
	
	/**
	 * Parses current subexpression of a regex by moving forward in it from the position it was called for, calling itself recursively for any subexpression one level deeper it encounters.
	 * 
	 * @param regex Regular expression whose subexpression needs to be parsed.
	 * @param j Position that the method should begin at, i.e the start of the subexpression.
	 * @return A pair of values: the constructed syntax tree and the position current subexpression ends at.
	 */
	public static Pair<SyntaxTree, Integer> parse(String regex, Integer j) {
		SyntaxTree L = null;
		try {
			while (regex.charAt(j) != -1) { // while(true) ?
				if (regex.charAt(j) != '|' && regex.charAt(j) != '*' && regex.charAt(j) != '(' && regex.charAt(j) != ')' && regex.charAt(j) != '+' && regex.charAt(j) != '?') {		//if a Character
					SyntaxNode leaf = new SyntaxNode(regex.charAt(j));
					
					if (j < regex.length() - 1) {
						if (regex.charAt(j+1) == '*' || regex.charAt(j+1) == '+' || regex.charAt(j+1) == '?') {
							SyntaxNode tmp = leaf;
							leaf = new SyntaxNode(regex.charAt(j+1));
							leaf.setLeftChild(tmp);
							j++;
						}
					}
					
					SyntaxTree leafTree = new SyntaxTree(leaf);
					j++;
					
					if (L != null) {
						SyntaxNode temp = new SyntaxNode('.');
						temp.setLeftChild(L.root);
						temp.setRightChild(leafTree.root);
						L.root = temp;
					} else {
						L = leafTree;
					}
				}
				if (regex.charAt(j) == '(') {						//if a (
					Pair<SyntaxTree, Integer> subexp;
					subexp = parse(regex, j+1);
					j = subexp.b;
					j++;
				
					if (L != null) {
						SyntaxNode temp = new SyntaxNode('.');
						temp.setLeftChild(L.root);
						temp.setRightChild(subexp.a.root);
						L.root = temp;
					} else {
						L = subexp.a;
					}
				}
				if (regex.charAt(j) == '|') {					//if a |
					Pair<SyntaxTree, Integer> subexp;
					subexp = parse(regex, j+1);
					j = subexp.b;
					
					SyntaxNode temp = new SyntaxNode('|');
					temp.setLeftChild(L.root);
					temp.setRightChild(subexp.a.root);
					L.root = temp;
				}
				if (regex.charAt(j) == '*' || regex.charAt(j) == '+' || regex.charAt(j) == '?') {				//if a *
					SyntaxNode temp = new SyntaxNode(regex.charAt(j));
					temp.setLeftChild(L.root);
					L.root = temp;
					j++;
				}
				if (regex.charAt(j) == ')') {			//if a )
					if (j < regex.length() - 1) {
						if (regex.charAt(j+1) == '*' || regex.charAt(j+1) == '+' || regex.charAt(j+1) == '?') {
							SyntaxNode tmp = L.getRoot();
							L = new SyntaxTree(new SyntaxNode(regex.charAt(j+1)));
							L.getRoot().setLeftChild(tmp);
							j++;
						}
					}
					Pair<SyntaxTree, Integer> subexp = new Pair<SyntaxTree, Integer>(L,j);
					return subexp;
				}
			}
		}
		catch(StringIndexOutOfBoundsException e) {
		}
		Pair<SyntaxTree, Integer> res = new Pair<SyntaxTree, Integer>(L,j);
		return res;
	}
}