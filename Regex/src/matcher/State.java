package matcher;

/**
 * Class State
 *
 * @author Nils Goeritz (313780), Patrick Finke (343628), Bella Naumova (318299)
 * @version 1.0
 */
public class State {
	/**
	 * Array containing transitions that start in this state
	 */
	private Transition[] trans;
	
	/**
	 * Default constructor
	 */
	public State() {
		this.trans = new Transition[0];
	}
	
	/**
	 * method to add transitions to an state
	 * @param e Destination of the transition
	 * @param s Symbol of the transition
	 */
	public void addTrans(State e, char s){
		Transition tr = new Transition(e,s);
		if (trans == null){
	      trans = new Transition[1];
	      trans[0] = tr;
	    }
	    else {
	      Transition ctrans[] = new Transition[trans.length + 1];
	      System.arraycopy(trans, 0, ctrans, 0, trans.length);
	      trans = ctrans;
	      trans[trans.length-1] = tr;
	    }
	}
	
	/**
	 * Getter for all transitions, that start in this state 
	 * @return All transitions, that start in this state
	 */
	public Transition[] getTrans(){
		return this.trans;
	}
	
	/**
	 * Setter for all transitions, that start in this state
	 * @param t Initial value for transitions that start in this state
	 */
	public void setTrans(Transition[] t){
		this.trans = t;
	}
}
