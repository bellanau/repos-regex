package matcher;

/**
 * Class Pair is generic and represents a pair of values. Its purpose is to allow us to return two values at once, namely a syntax tree and an integer, in the method {@link SyntaxTree#parse(String, Integer).}
 * @author Nils Goeritz (313780), Patrick Finke (343628), Bella Naumova (318299)
 * @version 1.0
 * @param <A> first data type
 * @param <B> second data type
 */
public class Pair<A,B> {
	/**
	 * first data value
	 */
	public A a;
	/**
	 * second data value
	 */
	public B b;
	
	/**
	 * Constructor
	 * @param a initial value for the first data value
	 * @param b initial value for the second data value
	 */
	public Pair(A a, B b) {
		this.a = a;
		this.b = b;
	}
}