package matcher;

/**
 * Class Automat
 *
 * @author Nils Goeritz (313780), Patrick Finke (343628), Bella Naumova (318299)
 * @version 1.0
 */
public class Automat {
	/**
	 * Start state of the automat
	 */
	private State start;
	/**
	 * End state of the automat
	 */
	private State end;
	
	/**
	 * Constructor of an empty automat
	 */
	public Automat(){
		start = new State();
		end = new State();
	}
	
	/**
	 * Constructor, that creates an automat out of an syntax tree
	 * @param tree SyntaxTree der in einen Automaten �bersetzt werden soll
	 */
	public Automat(SyntaxTree tree) {
		this.start = new State();
		this.end = new State();
		this.SyntaxTreeToAutomat(tree.getRoot(), this.start, this.end);
	}
	
	/**
	 * Getter for start
	 * @return Start
	 */
	public State getStart(){
		return this.start;
	}
	
	/**
	 * Setter for start
	 * @param t Start
	 */
	public void setStart(State t){
		this.start = t;
	}
	
	/**
	 * Getter for End
	 * @return end
	 */
	public State getEnd(){
		return this.end;
	}
	
	/**
	 * Setter for end
	 * @param t End
	 */
	public void setEnd(State t){
		this.end = t;
	}
	
	/**
	 * Method to transfer an syntax tree into an automat
	 * @param tree Syntax tree, that shall be transformed
	 * @param s Start state of the method
	 * @param e End state of the method
	 */
	public void SyntaxTreeToAutomat(SyntaxNode tree, State s, State e){
		if ((tree).isOperation()) {
			if ((tree).getOperation() == '.'){
				State m = new State();
				SyntaxTreeToAutomat(tree.getLeftChild(), s, m);
				SyntaxTreeToAutomat(tree.getRightChild(), m, e);
			} else if ((tree).getOperation() == '|'){
				SyntaxTreeToAutomat(tree.getLeftChild(), s, e);
				SyntaxTreeToAutomat(tree.getRightChild(), s, e);
			} else if ((tree).getOperation() == '*'){
				s.addTrans(e,'\u0000');
				e.addTrans(s,'\u0000');
				SyntaxTreeToAutomat(tree.getLeftChild(), s, e);
			} else if ((tree).getOperation() == '?'){
				s.addTrans(e,'\u0000');
				SyntaxTreeToAutomat(tree.getLeftChild(), s, e);
			} else if ((tree).getOperation() == '+'){
				e.addTrans(s,'\u0000');
				SyntaxTreeToAutomat(tree.getLeftChild(), s, e);
			}
		} else {
			s.addTrans(e, (tree).getOperation());
		}
	}
	
	/**
	 * Method to simulate an NFA set-based
	 * @param t Text that is to be simulated
	 * @return Array of OutputTuple
	 */
	public OutputTuple[] simulateSet(String t){
		OutputTuple[] ot = new OutputTuple[0];
		int n = t.length();
		for(int i=0; i<n; i++){
			State[] active = new State[1];
			active[0] = start;
			active = findAllEps(start, active);
			int j = i-1;
			while (active.length>0){
				if (containsAcceptedState(active)){
					OutputTuple[] cot = new OutputTuple[ot.length+1];
					System.arraycopy(ot, 0, cot, 0, ot.length);
					ot = cot;
					ot[ot.length-1] = new OutputTuple(i,j);
					// System.out.println("(" + i + ", " + j + ")");
				}
				j++;
				if (j >= n) {
					break;
				}
				active = updateActive(active, t.charAt(j));
			}
		}
		return ot;
	}
	
	/**
	 * Method to check, if an accepted state is in an array of states
	 * @param s Array of states
	 * @return Logical value
	 */
	private boolean containsAcceptedState(State[] s){
		for (int i=0; i<s.length; i++){
			if(s[i] == this.end){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Method to update the array of active states, when a symbol is read in
	 * @param s Array of active states before the symbol is read in
	 * @param z Symbol that is about to be read in
	 * @return Array of active states after the symbol is read in
	 */
	private State[] updateActive(State[] s, char z){
		State[] updated = new State[0];
		State[] up;
		for (int i=0; i<s.length; i++){
			for(int j=0; j<(((s [i]).getTrans()).length); j++){
				if(((s [i]).getTrans() [j]).getSymbol() == z){
					up = new State[1];
					up[0] = ((s [i]).getTrans() [j]).getTo();
					up = findAllEps(up[0], up);
					for(int l=0; l<up.length; l++){
						if(!(containsState(up[l], updated))){
						      State cupdated[] = new State[updated.length + 1];
						      System.arraycopy(updated, 0, cupdated, 0, updated.length);
						      updated = cupdated;
						      updated[updated.length-1] = up[l];
						}
					}
				}
			}
		}
		return updated;
	}
	
	/**
	 * Method to simulate an NFA path-based
	 * @param t Text that is to be simulated
	 * @return Array of OutputTuple
	 */
	public OutputTuple[] simulatePath(String t){
		OutputTuple[] ot = new OutputTuple[0];
		int n = t.length();
		for(int i=0; i<n; i++){
			State[] found = new State[1];
			found[0] = start;
			State[] rs = findAllEps(start, found);
			StackElement[] stack = new StackElement[rs.length];
			for(int l = 0; l<rs.length; l++){
				StackElement ns = new StackElement(rs[l], i-1);
				stack[l] = ns;
			}
			while (stack.length>0){
				State q = stack[0].getState();
				int j = stack[0].getPosition();
				StackElement[] cstack = new StackElement[stack.length-1];
				System.arraycopy(stack, 1, cstack, 0, stack.length-1);
				stack = new StackElement[cstack.length];
				stack = cstack;
				if (isAcceptedState(q)){
					OutputTuple[] cot = new OutputTuple[ot.length+1];
					System.arraycopy(ot, 0, cot, 0, ot.length);
					ot = cot;
					ot[ot.length-1] = new OutputTuple(i,j);
					//System.out.println("(" + i + ", " + j + ")");
				}
				for(int k=0; k<(q.getTrans()).length; k++){
					if (j+1<n){
						if ((q.getTrans() [k]).getSymbol() == t.charAt(j+1)){
							found = new State[1];
							found[0] = (q.getTrans() [k]).getTo();
							rs = findAllEps((q.getTrans() [k]).getTo(), found);
							for(int l = 0; l<rs.length; l++){
								StackElement ns = new StackElement(rs[l],j+1);
								if (!(containsStack(ns, stack))){
									cstack = new StackElement[stack.length+1];
									System.arraycopy(stack, 0, cstack, 0, stack.length);
									stack = cstack;
									stack[stack.length-1] = ns;
								}
							}
						}
					}
				}
			}
		}
		return ot;
	}
	
	/**
	 * Method to check, if an state is an accepted state
	 * @param q The state, that shall be tested
	 * @return Logical value
	 */
	private boolean isAcceptedState(State q){
		State find[] = new State[1];
		find[0] = q;
		find = findAllEps(q,find);
		return containsState(end, find);
	}
	
	/**
	 * Recursive method to find all states, that are also active, if a certain state is active
	 * @param s The active state
	 * @param found Array of found states, that are also active, if the state is active
	 * @return Array of all active states, that are also active, if the state is active
	 */
	private State[] findAllEps(State s, State[] found){	
		for(int k=0; k<(s.getTrans()).length; k++){
			if ((s.getTrans() [k]).getSymbol() == '\u0000'){
				if (!(containsState((s.getTrans() [k]).getTo(), found))){ 
					State cfound[] = new State[found.length + 1];
				    System.arraycopy(found, 0, cfound, 0, found.length);
				    found = cfound;
				    found[found.length-1] = (s.getTrans() [k]).getTo();
				    found = findAllEps((s.getTrans() [k]).getTo(), found);
				}
			}
		}
		return found;
	}
	
	/**
	 * Method to check, if a state is in an array of states
	 * @param s The state in question
	 * @param vec The array of states that is to be checked
	 * @return Logical value
	 */
	private boolean containsState(State s, State[] vec){
		for(int i=0; i<vec.length; i++){
			if (vec[i] == s){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Method to check, if a stack element is in an array of stack elements
	 * @param s The stack element in question
	 * @param vec The array of stack elements that is to be checked
	 * @return Logical value
	 */
	private boolean containsStack(StackElement s, StackElement[] vec){
		for(int i=0; i<vec.length; i++){
			if (vec[i] == s){
				return true;
			}
		}
		return false;
	}
}
