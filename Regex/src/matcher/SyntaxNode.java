package matcher;

/**
 * Class SyntaxNode represents syntax nodes, which syntax trees, defined in {@link SyntaxTree}, consist of.
 * 
 * @author Nils Goeritz (313780), Patrick Finke (343628), Bella Naumova (318299)
 * @version 1.0
 */
public class SyntaxNode {
	
	/**
	 * Operation/character stored in the node.
	 */
	private char operation;
	
	/**
	 * Left child of the node. If a node only has one child, the convention is to store it as a left one.
	 */
	private SyntaxNode left;
	
	/**
	 * Right child of the node.
	 */
	private SyntaxNode right;
	
	/**
	 * Constructs a new syntax node, for both children null is assigned.
	 * @param op Denotes operation/character the new node will store. Avaliable operations: (, ), *, |. Avaliable characters: any other symbol.
	 */
	public SyntaxNode(char op) {
		operation = op;
		left = null;
		right = null;
	}
	
	/**
	 * Getter for {@link #operation}.
	 * @return char
	 */
	public char getOperation() {
		return this.operation;
	}
	
	/**
	 * Getter for {@link #left}.
	 * @return SyntaxNode
	 */
	public SyntaxNode getLeftChild() {
		return this.left;
	}
	
	/**
	 * Getter for {@link #right}.
	 * @return SyntaxNode
	 */
	public SyntaxNode getRightChild() {
		return this.right;
	}
	
	/**
	 * Adds/changes the left child.
	 * @param child New value of left child.
	 */
	public void setLeftChild(SyntaxNode child) {
		this.left = child;
	}
	
	/**
	 * Adds/changes the right child.
	 * @param child New value of right child.
	 */
	public void setRightChild(SyntaxNode child) {
		this.right = child;
	}
	
	/**
	 * Removes the left child.
	 */
	public void removeLeftChild() {
		this.left = null;
	}
	
	/**
	 * Removes the right child.
	 */
	public void removeRightChild() {
		this.right = null;
	}
	
	/**
	 * Checks if the node contains an operation or a character. Relays on the fact that operations always have at least one child node and characters always have none. Used for ambiguous cases like "." (as in dot character or as in notation for concatenation).
	 * @return Logical value.
	 */
	public boolean isOperation() {
		return (this.left != null);
	}
}
