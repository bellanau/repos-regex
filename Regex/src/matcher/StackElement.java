package matcher;

/**
 * Class StackElement
 *
 * @author Nils Goeritz (313780), Patrick Finke (343628), Bella Naumova (318299)
 * @version 1.0
 */
public class StackElement {
	/** State in the system */
	private State state;
	/** Index */
	private int position;
	
	/**
	 * Constructor of the class
	 * @param t Initial value of the state
	 * @param p Initial value of the index
	 */
	public StackElement(State t, int p){
		this.state = t;
		this.position = p;
	}
	
	/**
	 * Getter for the state
	 * @return state
	 */
	public State getState(){
		return this.state;
	}
	
	/**
	 * Setter for the state
	 * @param s Initial value of the state
	 */
	public void setState(State s){
		this.state = s;
	}
	
	/**
	 * Getter for the index
	 * @return Index
	 */
	public int getPosition(){
		return this.position;
	}
	
	/**
	 * Setter for the index
	 * @param p Initial value of the index
	 */
	public void setPosition(int p){
		this.position = p;
	}
}
