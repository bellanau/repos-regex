package matcher;

/**
 * Class RegexMatcher
 *
 * @author Nils Goeritz (313780), Patrick Finke (343628), Bella Naumova (318299)
 * @version 1.0
 */
public class RegexMatcher {

	/**
	 * Performs a match using set based automat simulation.
	 * @param regex The regular expression to be matched.
	 * @param text The text to be matched.
	 * @return Instance of RegexMatchResult holding the starting position of the first-longest matched string and the string itself.
	 */
    public static RegexMatchResult matchSetBased(String regex, String text) {
      // build tree then automat
      SyntaxTree tree = SyntaxTree.regexToSyntaxTree(regex);
      Automat automat = new Automat(tree);
      
      OutputTuple[] output = automat.simulateSet(text); // run simulation
      
      // test no match
      if (output.length == 0) {
    	  return new RegexMatchResult(-1, new String());
      }
      
      // find best match
      OutputTuple best = output[0];
      for(int i = 1; i < output.length; i++) {
    	  if (output[i].getStartPosition() < best.getStartPosition()
    			  || (output[i].getStartPosition() == best.getStartPosition() && output[i].getEndPosition() > best.getEndPosition())
    			  || (best.getEndPosition() < best.getStartPosition())) {
    				  best = output[i];
    			  }
      }
      
      // extract matched text
      String matchedText = text.substring(best.getStartPosition(), best.getEndPosition() + 1);
      
      // return position and text
      return new RegexMatchResult(best.getStartPosition(), matchedText);
    }

	/**
	 * Performs a match using path based automat simulation.
	 * @param regex The regular expression to be matched.
	 * @param text The text to be matched.
	 * @return Instance of RegexMatchResult holding the starting position of the first-longest matched string and the string itself.
	 */
    public static RegexMatchResult matchPathBased(String regex, String text) {
    	// build tree then automat
        SyntaxTree tree = SyntaxTree.regexToSyntaxTree(regex);
        Automat automat = new Automat(tree);
        
        OutputTuple[] output = automat.simulatePath(text); // run simulation
        
        // test no match
        if (output.length == 0) {
      	  return new RegexMatchResult(-1, new String());
        }
        
        // find best match
        OutputTuple best = output[0];
        for(int i = 1; i < output.length; i++) {
      	  if (output[i].getStartPosition() < best.getStartPosition()
      			  || (output[i].getStartPosition() == best.getStartPosition() && output[i].getEndPosition() > best.getEndPosition())
      			  || (best.getEndPosition() < best.getStartPosition())) {
      				  best = output[i];
      			  }
        }
        
        // extract matched text
        String matchedText = text.substring(best.getStartPosition(), best.getEndPosition() + 1);
        
        // return position and text
        return new RegexMatchResult(best.getStartPosition(), matchedText);
    }

}
