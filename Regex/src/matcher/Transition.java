package matcher;

/**
 * Class Transition
 *
 * @author Nils Goeritz (313780), Patrick Finke (343628), Bella Naumova (318299)
 * @version 1.0
 */
public class Transition {
	/**
	 * Symbol of the transition
	 */
	private char symbol;
	/**
	 * Destination of the transition
	 */
	private State to;
	
	/**
	 * Constructor of an transition
	 * @param t Initial value of the destination of the transition
	 * @param s Initial value of the symbol of the transition
	 */
	public Transition(State t, char s){
		this.symbol = s;
		this.to = t;
	}
	
	/**
	 * Getter for the destination of the transition
	 * @return destination of the transition
	 */
	public State getTo(){
		return this.to;
	}
	
	/**
	 * Setter for the destination of the transition
	 * @param s Initial value for the destination of the transition
	 */
	public void setTo(State s){
		this.to = s;
	}
	
	/**
	 * Getter for the symbol of the transition
	 * @return symbol of the transition
	 */
	public char getSymbol(){
		return this.symbol;
	}
	
	/**
	 * Setter for the symbol of the transition
	 * @param s Initial value for the symbol of the transition
	 */
	public void setSymbol(char s){
		this.symbol = s;
	}
	
}
