package matcher;

/**
 * Class OutputTuple
 *
 * @author Nils Goeritz (313780), Patrick Finke (343628), Bella Naumova (318299)
 * @version 1.0
 */
public class OutputTuple {
	/**
	 * Start position of the OutputTuple
	 */
	private int startPosition;
	/**
	 * End position of the OutputTuple
	 */
	private int endPosition;
	
	
	/**
	 * Constructor of an OutputTuple
	 * @param i Initial value for the start position
	 * @param j Initial value for the end position
	 */
	public OutputTuple(int i, int j){
		this.startPosition = i;
		this.endPosition = j;
	}
	
	/**
	 * Getter for the start position
	 * @return Start position
	 */
	public int getStartPosition(){
		return this.startPosition;
	}
	
	/**
	 * Getter for the end position
	 * @return End position
	 */
	public int getEndPosition(){
		return this.endPosition;
	}
	
}
