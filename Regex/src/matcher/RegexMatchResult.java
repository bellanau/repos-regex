package matcher;

/**
 * Class RegexMatchResult
 *
 * @author Nils Goeritz (313780), Patrick Finke (343628), Bella Naumova (318299)
 * @version 1.0
 */
public class RegexMatchResult {

	/**
	 * starting position of the matched text
	 */
    private int startingPosition;
    
    /**
     * the matched text
     */
    private String matchedString;

	/**
	 * Constructor
	 * @param i starting position
	 * @param s matched string
	 */
    public RegexMatchResult(int i, String s) {
        startingPosition = i;
        matchedString = s;
    }

    /**
     * Getter
     * @return starting position
     */
    public int getStartingPosition() {
        return startingPosition;
    }

    /**
     * getter
     * @return matched string
     */
    public String getMatchedString() {
        return matchedString;
    }

    /**
     * Print the information contained within this instance, nicely formatted.
     */
    public void print() {
        if (startingPosition == -1) {
            System.out.println("There is no match!");
        } else {
            System.out.println("Matching position: " + startingPosition);
            System.out.println("Matched substring: " + matchedString);
        }
    }

}